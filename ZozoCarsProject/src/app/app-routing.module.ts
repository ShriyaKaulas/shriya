import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from "./screens/home/home.component";
import {LoginComponent} from "./screens/login/login.component";
import {AboutComponent} from "./screens/about/about.component";
import {ContactUsComponent} from "./screens/contact-us/contact-us.component";
import {ErrorComponent} from "./screens/error/error.component";
import {HelpComponent} from "./screens/help/help.component";
import{AccountComponent} from "./screens/account/account.component";
import{SignUpComponent} from "./screens/sign-up/sign-up.component";
import{HeaderComponent} from "./shared/header/header.component";
import {FooterComponent} from "./shared/footer/footer.component";

const routes: Routes = [{
  path: "",
  component: HomeComponent,
  pathMatch: "full"
},
{
  path: "home",
  component: HomeComponent,
},
{
  path: "login",
  component: LoginComponent,
},
{
  path: "about",
  component: AboutComponent,
},
{
  path: "contact-us",
  component: ContactUsComponent,
},
{
  path: "Sign-up",
  component: SignUpComponent,
},
{
  path: "Account",
  component: AccountComponent,
},
{
  path: "Help",
  component: HelpComponent,
},
  
{
  path: "**",
  component: ErrorComponent,
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
