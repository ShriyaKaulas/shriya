import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './screens/home/home.component';
import { AboutComponent } from './screens/about/about.component';
import { ContactUsComponent } from './screens/contact-us/contact-us.component';
import { SignUpComponent } from './screens/sign-up/sign-up.component';
import { LoginComponent } from './screens/login/login.component';
import { HelpComponent } from './screens/help/help.component';
import { AccountComponent } from './screens/account/account.component';
import { ErrorComponent } from './screens/error/error.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import {APP_BASE_HREF} from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ContactUsComponent,
    SignUpComponent,
    LoginComponent,
    HelpComponent,
    AccountComponent,
    ErrorComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [{provide:APP_BASE_HREF, useValue:''}],
  bootstrap: [AppComponent]
})
export class AppModule { }
